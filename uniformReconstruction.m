function  [ results ] = uniformReconstruction( testImages, nofProj )

    nofScales = length(testImages);
    results = cell(1,length(testImages));
    
    for j=1:nofScales
        results{j}.scale = testImages{j}.scale;
        for k=1 : length(testImages{1}.images)
            [reconstruction, angles, errors, PSNR] = performSingleImage(testImages{j}.images{k}, nofProj );
            results{j}.images{k}=reconstruction;
            results{j}.angles{k}=angles;
            results{j}.errors{k}=errors;
            results{j}.PSNR{k}=PSNR;
        end
    end
end

function [reconstruction, angles, errors, psnr] = performSingleImage(inputImage, maxNumOfProj )
    
    imgSize = size(inputImage, 1);
    angles = linspace(0,179,maxNumOfProj);
    errors = zeros(1, maxNumOfProj);
    P1 = radon(inputImage, angles(1));
    P2 = radon(inputImage, angles(2));
    %This is the way in matlab to perform one step of FBP for a certain angle
    firstR = iradon([P1 P1], [angles(1) angles(1)], 'linear', imgSize) / 2;
    secondR = iradon([P2 P2], [angles(2) angles(2)], 'linear', imgSize) / 2;
    %secondR = (secondR + firstR);
    prevRec = secondR + firstR;

    E1 = log10(sumabs((prevRec) - firstR).^2);
    errors(1)= 0;
    errors(2)= E1;

    aNOP=2;
    
    %A cikkbeli algoritmus szerint ezt nem szamolja ujra minden iteracioban
    %Eref=mean(errors(1, 1:aNOP));
    
    while angles(aNOP) < 179 && aNOP<maxNumOfProj
        aNOP=aNOP+1;
        nextAngle=angles(aNOP);
        R=radon(inputImage, nextAngle);
        actRec = iradon([R R], [nextAngle, nextAngle], 'linear', imgSize)/2;
        reconstruction = (prevRec + actRec);
        Ecurr = log10(sumabs((reconstruction) - (prevRec)).^2);
        errors(aNOP)=Ecurr;
        prevRec=reconstruction;
    end
    reconstruction=reconstruction/(aNOP);
    
    mse=sum(sum((inputImage-reconstruction).^2))*1/imgSize^2;
    psnr = 10*log10(1/mse);    
end
