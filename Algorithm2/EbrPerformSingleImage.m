function [reconstruction, angles, stepSizes, errors, psnr, rmr] = EbrPerformSingleImage(inputImage, initStep, maxNumOfProj, ny, initAngle )
    
    imgSize = size(inputImage, 1);

    % * * * SIRT * * *
    mask=ones(imgSize,imgSize);
    start=zeros(imgSize,imgSize);
    start(:,:) = 0.5;
    % * * * *  * * * *
    
    angles = zeros(1, maxNumOfProj);
    errors = zeros(1, maxNumOfProj);
    angles(1) = initAngle;
    angles(2) = initAngle + initStep;
    % just for 1st reconsctruction
    firstAngles(1)=initAngle-initStep;
    firstAngles(2)=initAngle;
    firstAngles(3)=angles(2);
    
    P0 = radon(inputImage, initAngle-initStep);
    P1 = radon(inputImage, firstAngles(1:2));
    P2 = radon(inputImage, firstAngles);
    %This is the way in matlab to perform one step of FBP for a certain angle
    %*** firstR = iradon([P1 P1], [angles(1) angles(1)], 'linear', imgSize) / 2;
    %*** secondR = iradon([P2 P2], [angles(2) angles(2)], 'linear', imgSize) / 2;
    %*** from now we use SIRT instead of FBP ***
    zeroR = SIRTBound(P0, firstAngles(1), start, mask, 0.1, 100);
    firstR = SIRTBound(P1, firstAngles(1:2), start, mask, 0.1, 100);
    secondR = SIRTBound(P2, firstAngles, start, mask, 0.1, 100);
        
    E0 = log10(sumabs(firstR - zeroR).^2);
    errors(1)= E0;

    E1 = log10(sumabs(secondR - firstR).^2);
    errors(2)= E1;

    aNOP=1;
    stepSizes=zeros(1, maxNumOfProj-1);
    stepSizes(1) = angles(2)-angles(1);
    
    %A cikkbeli algoritmus szerint ezt nem szamolja ujra minden iteracioban
    Eref=mean(errors(1, 1:2));
    %reconstruction = secondR;
    prevRec = secondR;
    
    while angles(aNOP+1) < (179 + initAngle) && aNOP+1<maxNumOfProj
        %Eref = mean(errors(1, 1:aNOP));
        aNOP = aNOP + 1;
        deltaE = (errors(aNOP) - Eref)/errors(aNOP);
        avgStepSize = mean(stepSizes(1, 1:aNOP-1));
        deltau = avgStepSize*ny*deltaE;
        if deltau > 4
            deltau=4;
        elseif deltau < -4
            deltau = -4;
        end
        
        stepSizes(aNOP) = initStep - deltau;
        nextAngle=angles(aNOP) + stepSizes(aNOP);
        %itt mar novelni erdemes
        angles(aNOP+1)=nextAngle;
        R=radon(inputImage, angles(1:aNOP+1));     
        
        reconstruction = SIRTBound(R, angles(1:aNOP+1), start, mask, 0.1, 100);
        Ecurr = log10(sumabs(reconstruction - prevRec).^2);
        errors(aNOP+1)=Ecurr;
        
        if angles(aNOP+1) >= (179+initAngle) || aNOP+1 >= maxNumOfProj
            reconstruction=prevRec;
        else
            prevRec=reconstruction;
        end
    end
    
    mse=sum(sum((inputImage-reconstruction).^2))*1/imgSize^2;
    psnr = 10*log10(1/mse);
    
    % RME- érték számítása a rekonstruált kép binarizaltjaval
    rmr=RMR(inputImage, reconstruction > 0.5);
    %ha a vetuletek szama kisebb lenne mint az elore meghatarozott
    %maximalis vetuletszam akkor ezeket az adatokat erdemes akkora tombbe
    %athelyezni a kesobbi korrekt hiba, PSNR osszehasonlitashoz...
    if(aNOP<maxNumOfProj)
        angles=angles(1:aNOP);
        errors=errors(1:aNOP);
        stepSizes=stepSizes(1:aNOP);
    end
end
