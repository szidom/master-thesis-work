% olyan kepekre mukodik, melyeknek egyforma a magassaga es a szelessege,
% illetve paros szamu pixel az oldalhossz, de ezt nem nagy cucc modositani,
% csak neked ugyis 256x256-os kepeid lesznek most elso korben
size = 256;

for alpha=0:179
    img=zeros(size, size);  % demohoz egy fekete kep
    % ez az if resz azert kell, mert a mintavetelezes attol fugg, hogy az
    % egyenes milyen szogben van. ettol fuggoen modositani kell, hogy
    % mindig a teljes kepen vegig huzodjon az egyenes
    % (ez a csomo (size/2) meg tarsai azert vannak, mert ugye (0,0)-hoz
    % kepest szamol eredetileg, nekunk meg a kozepponton kell, hogy 
    % athaladjon)
    if (alpha >= 0 && alpha < 45) || (alpha > 135 && alpha <= 179)
        y = (-(size/2-1):size/2); % mintavetelezeshez a pontok
        x=round(-y/cot(deg2rad(alpha))); % egyenes egyenlet
        % egyenes egyenlet eredetileg amibol szamoltam: x*cos(a)+y*sin(a)=0
    else
        x = (-(size/2-1):size/2);
        y=round(-x*cot(deg2rad(alpha)));
    end
    
    % itt rajzoljuk ra a letrehozott kepre a pontokat
    % neked egyaltalan nem kell semmit sem rajzolnod igyesen sehova, mert a
    % kovetkezo ciklus megmondja, hogy mely pontok tartoznak az egyeneshez,
    % szoval csak fogod a fourier-es kepet, is az alabbi ciklus altal
    % meghatarozott pontok intenzitasait osszeadod
    % ezzel megusszuk a kepszorzast meg meg ilyeneket
    for i=1:size
        if y(i)+(size/2) >= 1 && y(i)+(size/2) <= size
            img(x(i)+(size/2), y(i)+(size/2))=1;    % EZEK A VONAL KOORDINATAI
        end
    end
    
    imshow(img,[])
end