% This script is meant to find the cases when the number of projections 
% are the same for all the 3 methods, and collect the RME values of those.
rmeComp = zeros(9,10);
k=1;
figure
set(gcf, 'Units', 'Normalized','OuterPosition', [0 0 1 1]);
for j=1:22
    nn=k;
    for jj=1:180
        if alg2UncResults{1,j}.projNum{jj} == alg2Results{1,j}.projNum{jj} && alg2UncResults{1,j}.projNum{jj} == alg1Results{1,j}.projNum{jj}
            rmeComp(1,k)=alg2UncResults{1,j}.rmr{jj};
            rmeComp(2,k)=alg2Results{1,j}.rmr{jj};
            rmeComp(3,k)=alg2Results{1,j}.projNum{jj};
            rmeComp(4,k)=alg1Results{1,j}.rmr{jj};
            rmeComp(5,k)=alg1Results{1,j}.projNum{jj};
            rmeComp(6,k)=rmeComp(1,k)<rmeComp(2,k);
            rmeComp(7,k)=rmeComp(1,k)<rmeComp(4,k);
            rmeComp(8,k)=j;
            rmeComp(9,k)=jj;
            k=k+1;
        end
    end
    if nn~=k
        x=1:k-nn;
        plot(x, rmeComp(4, nn:k-1), 'or', x, rmeComp(2, nn:k-1), 'sb', x, rmeComp(1, nn:k-1), 'dg')
        ylabel('RME');
        legend('Alg1','Alg2','Unc');
        fname=sprintf('/results/compareAlgs/sameProjNum/RMEs%d.png',j);
        saveas(gcf,[pwd, fname]);    
    end
end

sum(rmeComp(1,:)<rmeComp(2,:))
sum(rmeComp(1,:)<rmeComp(4,:))
% sum(rmeComp(1,:)==rmeComp(2,:))
% sum(rmeComp(1,:)==rmeComp(4,:))

figure
set(gcf, 'Units', 'Normalized','OuterPosition', [0 0 1 1]);
x=1:numel(rmeComp(1,:));
plot(x, rmeComp(4,:), '.r', x, rmeComp(2,:), '.b', x, rmeComp(1,:), '.g')
ylabel('RME');
legend('Alg1','Alg2','Unc');
