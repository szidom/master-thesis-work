nfunction plotCompByInitStep(inputImages, unInputImages)
    numOfImagesPerScale=length(inputImages{1}.images);
    nofScales = length(inputImages);
    colors=['ymcgbwk'];
    % it returns with length of ny-s 
    dim2 = size(inputImages{1}.initStep,3);
    dataToPlot=zeros(numOfImagesPerScale, dim2+1 ); 
     
    for j=1:length(inputImages{1}.ny(1,:))
         figure;
         %bar(1:numOfImagesPerScale, cellfun(@mean, unInputImages{j}.errors), 'r');
         dataToPlot(:, 1) = cellfun(@mean, unInputImages{1}.errors);
         stringforLegends=['Uniform Projections'];
         for k=1:length(inputImages{1}.initStep(1,1,:))         
            datatocollect = zeros(1, numOfImagesPerScale);
            for i=1:numOfImagesPerScale
                datatocollect(i) = cellfun(@mean, inputImages{1}.errors(i,j,k));
                
            end                
            dataToPlot(:,k+1) = datatocollect';
            stringforLegends=[stringforLegends, strcat('initStep=', string(inputImages{1}.initStep{1,1,k})) ];
            %stringforLegends=[stringforLegends, strcat('ny=', string(inputImages{j}.ny{1,k})) ];
         end
         bar(dataToPlot);
         title(strcat(string(inputImages{1}.scale),' scale Images, ny=',string(inputImages{1}.ny{1,j})));         
         xlabel('Number of Images');
         ylabel('Error');
         legend(stringforLegends);
     end
end
