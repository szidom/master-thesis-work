function  [ results ] = EBReconstruct( testImages, initStep, maxNumOfProj, nys )
%EBRECONSTRUCT Adaptive angle chooser version of FBP
%   testImages a structure with the images and their scalesa 
%   initStep which means an angle between the first 2 projection.
%   maxNumOfProj is the number of the max projections for a reconstruction
%   nys is a vector, the set of coupleing parameter between the step size and counted error for the previous projection
%   The algorithm will terminate if it reaches the max number of projections,
%   or the projection angle is greater than 179.
%   We allocate an array for the angles and for the errors to, because
%   dynamic allocation in loop is not very good thing.

    nofScales = length(testImages);
    results = cell(1,length(testImages));
    initAngle = 0;
    
    for j=1:nofScales
        results{j}.scale = testImages{j}.scale;
        for k=1 : length(testImages{1}.images)
            for kk=1 : length(nys)
                for kkk = 1 : length(initStep)
                    [reconstruction, angles, stepSizes, errors, PSNR, rmr] = ...
                        EbrPerformSingleImage(testImages{j}.images{k}, initStep(kkk), maxNumOfProj, nys(kk), initAngle );
                    results{j}.images{k, kk, kkk} = reconstruction;
                    results{j}.angles{k, kk, kkk} = angles;
                    results{j}.stepSizes{k, kk, kkk} = stepSizes;
                    results{j}.errors{k, kk, kkk} = errors;
                    results{j}.initStep{k, kk, kkk} = initStep(kkk);
                    results{j}.PSNR{k,kk,kkk} = PSNR;
                    results{j}.rmr{k,kk,kkk}=rmr;
                    results{j}.numOfProjs{k,kk,kkk} = length(angles);                    
                end
                results{j}.ny{k, kk} = nys(kk);
            end
        end
    end
end

