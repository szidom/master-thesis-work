function [reconstruction, angles, stepSizes, errors, psnr, rmr] = UncertainPerformSingleImage(inputImage, initStep, maxNumOfProj, ny, initAngle )
    
    imgSize = size(inputImage, 1);

    % * * * SIRT * * *
    mask=ones(imgSize,imgSize);
    start=zeros(imgSize,imgSize);
    start(:,:) = 0.5;
    % * * * *  * * * *
    
    angles = zeros(1, maxNumOfProj);
    errors = zeros(1, maxNumOfProj);
    angles(1) = initAngle;
    angles(2) = initAngle + initStep;
    % just for 1st reconsctruction
    firstAngles(1)=initAngle-initStep;
    firstAngles(2)=initAngle;
    firstAngles(3)=angles(2);
    
    P0 = radon(inputImage, initAngle-initStep);
    P1 = radon(inputImage, firstAngles(1:2));
    P2 = radon(inputImage, firstAngles);
    %This is the way in matlab to perform one step of FBP for a certain angle
    %*** firstR = iradon([P1 P1], [angles(1) angles(1)], 'linear', imgSize) / 2;
    %*** from now we use SIRT instead of FBP ***
    zeroR = SIRTBound(P0, firstAngles(1), start, mask, 0.1, 100);
    firstR = SIRTBound(P1, firstAngles(1:2), start, mask, 0.1, 100);
    %*** secondR = iradon([P2 P2], [angles(2) angles(2)], 'linear', imgSize) / 2;
    secondR = SIRTBound(P2, firstAngles, start, mask, 0.1, 100);
        
    %%E0 = log10(sumabs((firstR + secondR)/2 - firstR).^2);
    %E0 = log10(sumabs(getUnc(P1, firstR) - getUnc(P0, zeroR)).^2);
    errors(1) = getUnc(P1, firstR);
    E0 = errors(1) - getUnc(P0, zeroR);
%     errors(1)= E0;

    %*** E1 = log10(sumabs(secondR - firstR).^2);
    %E1 = log10(sumabs(getUnc(P2, secondR) - E0).^2) ;
    errors(2) = getUnc(P2, secondR);
    E1 = errors(2) - errors(1);
%     errors(2) = E1;

    %Eref = mean(errors);

    aNOP=1;
    stepSizes = zeros(1, maxNumOfProj-1);
    stepSizes(1) = angles(2)-angles(1);
    
    %A cikkbeli algoritmus szerint ezt nem szamolja ujra minden iteracioban
    Eref = (E0 + E1)/2;
    reconstruction = secondR;
    
    while angles(aNOP+1) < (179 + initAngle) && aNOP+1<maxNumOfProj
        %Eref = mean(errors(1, 1:aNOP));
        aNOP = aNOP + 1;
        
        curErr = errors(aNOP) - errors(aNOP-1); 
        
        deltaE = (curErr - Eref)/curErr;
%         deltaE = (errors(aNOP) - Eref)/errors(aNOP);
        avgStepSize = mean(stepSizes(1, 1:aNOP-1)); % kiiratni ennek az erteket. folyamatosan + e?, biztos?
        deltau = avgStepSize*ny*deltaE;
        if deltau > 6
            deltau = 6;
        elseif deltau < -6
            deltau = -6;
        end
        
        stepSizes(aNOP) = initStep - deltau;
        nextAngle = angles(aNOP) + stepSizes(aNOP);
        %itt mar novelni erdemes
        
        angles(aNOP+1)=nextAngle;
        %uncErr = getUnc(R, actRec); 
        %Ecurr =  log10(sumabs(getUnc(R, actRec)-errors(aNOP)).^2);    
%         Ecurr = getUnc(R, actRec)-errors(aNOP);
        if angles(aNOP+1) < (179 + initAngle) && aNOP+1<maxNumOfProj 
            R = radon(inputImage, angles(1:aNOP+1));
            actRec = SIRTBound(R, angles(1:aNOP+1), start, mask, 0.1, 100);
            reconstruction = actRec;
            errors(aNOP+1) = getUnc(R, actRec);
        end
    end
    %reconstruction=reconstruction/(aNOP);
    
    mse=sum(sum((inputImage-reconstruction).^2))*1/imgSize^2;
    psnr = 10*log10(1/mse);
    
    % RME- érték számítása a rekonstruált kép binarizaltjaval
    rmr=RMR(inputImage, reconstruction > 0.5);
    %ha a vetuletek szama kisebb lenne mint az elore meghatarozott
    %maximalis vetuletszam akkor ezeket az adatokat erdemes akkora tombbe
    %athelyezni a kesobbi korrekt hiba, PSNR osszehasonlitashoz...
    if(aNOP<maxNumOfProj)
        angles=angles(1:aNOP);
        errors=errors(1:aNOP);
        stepSizes=stepSizes(1:aNOP);
    end
end
