function  [ results ] = SRBReconstruct( testImages, initStep, maxNumOfProj, nys )
%SRBRECONSTRUCT Adaptive angle chooser version of FBP based on
%spatial spectral richness
%   testImages a structure with the images and their scalesa 
%   initStep which means an angle between the first 2 projection.
%   maxNumOfProj is the number of the max projections for a reconstruction
%   nys is a vector, the set of coupleing parameter between the step size and counted error for the previous projection
%   The algorithm will terminate if it reaches the max number of projections,
%   or the projection angle is greater than 179.
%   In advance we allocate an array for the angles and for the richnesses
%   also, because dynamic allocation in a loop is not a good practice.

    nofScales = length(testImages);
    results = cell(1,length(testImages));
    
    for j=1:nofScales
        results{j}.scale = testImages{j}.scale;
        for k=1 : length(testImages{1}.images)
            for kk=1 : length(nys)
                for kkk = 1 : length(initStep)
                    [reconstruction, angles, stepSizes, PSNR] = ...
                        performSingleImage(testImages{j}.images{k}, initStep(kkk), maxNumOfProj, nys(kk) );
                    results{j}.images{k, kk, kkk} = reconstruction;
                    results{j}.angles{k, kk, kkk} = angles;
                    results{j}.stepSizes{k, kk, kkk} = stepSizes;
                    results{j}.PSNR{k, kk, kkk} = PSNR;
                    results{j}.initStep{k, kk, kkk} = initStep(kkk);
                end
                results{j}.ny{k, kk} = nys(kk);
            end
        end
    end
end

function [reconstruction, angles, stepSizes, psnr] = performSingleImage(inputImage, initStep, maxNumOfProj, ny )
    % in the beginning we take the first 2 projection in order to calculate
    % spectral richness and then we continue in a loop
    imgSize = size(inputImage, 1);
    angles = zeros(1, maxNumOfProj);
    richnesses = zeros(1, maxNumOfProj);
    angles(1) = 0;
    angles(2) = initStep;
    
    P0 = radon(inputImage, 0);
    %This is the way in matlab to perform one step of FBP for a certain angle
    b0 = iradon([P0 P0], [0, 0], 'none',imgSize)/2;
    
    b2d=fft2(b0);
    
    W=dftmtx(imgSize);
    B0=W*b2d*W';
    
    %this is very ugly and slow need to be optimized, basically the paralel
    %line to the projected data
    %I=imresize(imrotate(eye(imgSize), 45), [imgSize, imgSize], 'box').*2;
    %I=full(spones(I));
    
    %tmp=abs(B0.*I);
    %since we start with vertical projection we take the horizontal line of
    % image in the furier space which is going to be the middle line of the
    % furier image
    tmp=abs(B0(imgSize/2,1:imgSize));
    %J0=sum(sum(tmp),2);
    %these values just need to be summed up
    J0=sum(tmp);
    
    R = radon(inputImage, [0, initStep]);
    b1 = iradon(R, [0, initStep], 'none',imgSize);
    b2d=fft2(b1);
    
    % W-t eleg egyszer letrehozni , meg I-t is hiszen a projekcio merete
    % mindig ugyanaz lesz
    B1=W*b2d*W';
    
    %G=imresize(imrotate(I, initStep),[imgSize,imgSize],'box');
    %G=full(spones(G));
    [X,Y]=GetRotLine(imgSize,initStep);
    
    %tmp=abs(B1.*G);
    %J1=sum(sum(tmp),2);
    J1=0;
    for l=1:imgSize
        if(X(l)~=0) %&& Y(l)~=0) -tulzott elovigyazatossag
            J1=J1+abs(B1(X(l),Y(l)));
        end
    end
   
    richnesses(1)= J0;
    richnesses(2)= J1;
    
    %actual number of prosumjections
    aNOP=2;
    stepSizes=zeros(1, maxNumOfProj-1);
    stepSizes(1) = angles(2);
    
    while angles(aNOP) < 179 && aNOP<maxNumOfProj
        Javg=mean(richnesses(1:aNOP));        
        deltaJ = (richnesses(aNOP) - Javg)/richnesses(aNOP);
        avgStepSize = mean(stepSizes(1:aNOP-1));
        deltau = avgStepSize*ny*deltaJ;
        if deltau>4
            deltau=4;
        end
        
        stepSizes(aNOP) = avgStepSize - deltau;
        nextAngle=angles(aNOP) + stepSizes(aNOP);
        %itt mar novelni erdemes
        aNOP = aNOP + 1;
        angles(aNOP) = nextAngle;
        R = radon(inputImage, angles(1:aNOP));
        bn = iradon(R, angles(1:aNOP), 'none', imgSize);
        %bn=reconstruction'*R;
        b2d = fft2(bn);
        
        Bn=W*b2d*W';
        %G=imresize(imrotate(I, angles(aNOP)),[imgSize,imgSize],'box');
        %G=full(spones(G));
        %tmp=abs(Bn.*G);
        [X,Y]=GetRotLine(imgSize, angles(aNOP));
        J1=0;
        for l=1:imgSize
            if(X(l)~=0) %&& Y(l)~=0) -tulzott elovigyazatossag
                J1=J1+abs(Bn(X(l),Y(l)));
            end
        end        
        richnesses(aNOP)=J1;
    end
    
    reconstruction=iradon(R,angles(1:aNOP),'linear', imgSize);
    %angles
    mse=sum(sum((inputImage-reconstruction).^2))*1/imgSize^2;
    psnr = 10*log10(1/mse);
    %ha a vetuletek szama kisebb lenne mint az elore meghatarozott
    %maximalis vetuletszam akkor ezeket az adatokat erdemes akkora tombbe
    %athelyezni a kesobbi korrekt hiba osszehasonlitashoz...
    if(aNOP<maxNumOfProj)
        angles=angles(1:aNOP);
    end
end
