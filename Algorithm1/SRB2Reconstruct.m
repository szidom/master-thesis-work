function  [ results ] = SRB2Reconstruct( testImages, initStep, maxNumOfProj, nys )
%SRBRECONSTRUCT Adaptive angle chooser version of FBP based on
%spatial spectral richness
%   testImages a structure with the images and their scalesa 
%   initStep which means an angle between the first 2 projection.
%   maxNumOfProj is the number of the max projections for a reconstruction
%   nys is a vector, the set of coupleing parameter between the step size 
%   and counted error for the previous projection
%   The algorithm will terminate if it reaches the max number of projections,
%   or the next projection angle is greater than 179.
%   In advance we allocate an array for the angles and for the richnesses
%   also, because dynamic allocation in a loop is not a good practice.

    nofScales = length(testImages);
    results = cell(1,length(testImages));
    initAngle = 0;
    
    for j=1:nofScales
        results{j}.scale = testImages{j}.scale;
        for k=1 : length(testImages{1}.images)
            for kk=1 : length(nys)
                for kkk = 1 : length(initStep)
                    [reconstruction, angles, stepSizes, PSNR, rmr] = ...
                        SrbPerformSingleImage(testImages{j}.images{k}, initStep(kkk), maxNumOfProj, nys(kk), initAngle );
                    results{j}.images{k, kk, kkk} = reconstruction;
                    results{j}.angles{k, kk, kkk} = angles;
                    results{j}.stepSizes{k, kk, kkk} = stepSizes;
                    results{j}.PSNR{k, kk, kkk} = PSNR;
                    results{j}.initStep{k,kk, kkk} = initStep(kkk);
                    results{j}.rmr{k,kk,kkk}=rmr;
                    results{j}.numOfProjs{k,kk,kkk} = length(angles);
                end
                results{j}.ny{k, kk} = nys(kk);
            end
        end
    end
end

