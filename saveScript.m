%% Saving Alg1 results
%This script is meant to save the results of the 1st and 2nd algorithm in a
%more memory-efficient way. It assumes that we eather already run the 1st
%and 2nd algorithms via scriptAlg1 and scriptAlg2 or we loaded a matfile
%where can be found the results (in cell-arrays) of those.
% The script saves the result images in an appropriate folder structure and
% the corresponding data cell-arrays in a mat file.
for j=1:length(results1{1}.initStep(1,1,:))
    curdir=strcat('results/alg1/init', num2str(results1{1}.initStep{1,1,j}));
    for jj=1:length(results1{1}.ny(1,:))
        curdd=strcat(curdir,'/ny', num2str(results1{1}.ny{1,jj}));
        if ~(exist(curdd,'dir'))
            mkdir(curdd);
        end
        for jjj=1:length(results1{1}.images(:,jj,j))
            curfile=strcat(curdd, '/img',num2str(jjj),'.png');
            imwrite(results1{1}.images{jjj,jj,j}, curfile);
        end
    end
end
results1{1}.images=[];

% uniform - min and max projection number

curdir='results/alg1/uniMin';
if ~exist(curdir,'dir')
	mkdir(curdir);
end
for j=1:length(results1{2}{1}.image)
    curfile=strcat(curdir, '/img',num2str(j),'.png');
    imwrite(results1{2}{1}.image{j}, curfile);
end
results1{2}{1}.image=[];

curdir='results/alg1/uniMax';
if ~exist(curdir,'dir')
	mkdir(curdir);
end
for j=1:length(results1{3}{1}.image)
    curfile=strcat(curdir, '/img',num2str(j),'.png');
    imwrite(results1{3}{1}.image{j}, curfile);
end
results1{3}{1}.image=[];
%% Saving Alg2 results
for j=1:length(results2{1}.initStep(1,1,:))
    curdir=strcat('results/alg2/init', num2str(results2{1}.initStep{1,1,j}));
    for jj=1:length(results2{1}.ny(1,:))
        curdd=strcat(curdir,'/ny', num2str(results2{1}.ny{1,jj}));
        if ~exist(curdd,'dir')
            mkdir(curdd);
        end
        for jjj=1:length(results2{1}.images(:,jj,j))
            curfile=strcat(curdd, '/img',num2str(jjj),'.png');
            imwrite(results2{1}.images{jjj,jj,j}, curfile);
        end
    end
end
results2{1}.images=[];

% uniform - min and max projection number

curdir='results/alg2/uniMin';
if ~exist(curdir,'dir')
	mkdir(curdir);
end
for j=1:length(results2{2}{1}.image)
    curfile=strcat(curdir, '/img',num2str(j),'.png');
    imwrite(results2{2}{1}.image{j}, curfile);
end
results2{2}{1}.image=[];

curdir='results/alg2/uniMax';
if ~exist(curdir,'dir')
	mkdir(curdir);
end
for j=1:length(results2{3}{1}.image)
    curfile=strcat(curdir, '/img',num2str(j),'.png');
    imwrite(results2{3}{1}.image{j}, curfile);
end
results2{3}{1}.image=[];
save('results/resDataWithoutImages.mat', 'results1', 'results2');