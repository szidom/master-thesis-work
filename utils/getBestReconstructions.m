%% Tesztkepek beolvasasa
% currfile = mfilename('fullpath');
% [pathstr,~,~]=fileparts(currfile);
% cd (pathstr);

addpath('TomoPhantoms');
addpath('Algorithm2/Uncertainty');
% addpath('utils');
addpath('results');
files=dir('TomoPhantoms/*.tif');
testImages = cell(1,1);
nofImages=length(files);

for j=1:nofImages
    testImages{1}.images{j} = imread(strcat('TomoPhantoms/',files(j).name));
end

%% Getting the best result of Alg1
bestAlg1Res=zeros(2,22);
for k=1:22
    minRMR=10;
    currRMR=0;
    minUrmr=10;
    currURMR=0;
    for kk=1:180
        currRMR = alg1Results{1,k}.rmr{kk};
        if currRMR < minRMR
            minRMR=currRMR;
            bestAlg1Res(1,k)=kk;
        end
        currURMR = alg1Results{1,k}.uRMR{kk};
        if currURMR < minUrmr
            minUrmr=currURMR;
            bestAlg1Res(2,k)=kk;
        end
    end
end

% Getting the best result of Alg2
% de az alg2 az meg nem volt teljesen jo mikor utoljara futott 
% UPD!! 01.28 utoljara futtatva helyesen :)
bestAlg2Res=zeros(2,22);
for k=1:22
    minRMR=1000;
    currRMR=0;    
    minUrmr=10;
    currURMR=0;
    for kk=1:180
        currRMR = alg2Results{1,k}.rmr{kk};
        if currRMR < minRMR
            minRMR=currRMR;
            bestAlg2Res(1,k) = kk;
        end
        currURMR = alg2Results{1,k}.uRMR{kk};
        if currURMR < minUrmr
            minUrmr=currURMR;
            bestAlg2Res(2,k) = kk;
        end
    end
end

% Getting the best Result of Alg2+Unc
bestUncRes=zeros(2,22);

for k=1:22
    minRMR=100.0;
    currRMR=0;
    minUrmr=10;
    currURMR=0;    
    for kk=1:180
        currRMR = alg2UncResults{1,k}.rmr{kk};
        if currRMR < minRMR
            minRMR=currRMR;
            bestUncRes(1,k)=kk;
        end
        currURMR = alg2UncResults{1,k}.uRMR{kk};
        if currURMR < minUrmr
            minUrmr=currURMR;
            bestUncRes(2,k)=kk;
        end
    end
end
%% Reconstruct and getting the image Alg1

 for k=1:22
     
    currImg=testImages{1}.images{k};  
    imgSize = size(currImg, 1);
    % * * * SIRT * * *
    mask=ones(imgSize,imgSize);
    start=zeros(imgSize,imgSize);
    start(:,:) = 0.5;
    % * * * *  * * * *
    
    
        angles = alg1Results{k}.angles{1,bestAlg1Res(k)};
        P = radon(currImg, angles);
        reconstruction = SIRTBound(P, angles, start, mask, 0.1, 100);
        fname=strcat('results/alg1SirtIncrAngles/bestRecs/img',num2str(k),'pn', num2str(alg1Results{k}.projNum{bestAlg1Res(1,k)}),'deg',num2str(bestAlg1Res(1,k)),'.png');
        imwrite(reconstruction > 0.5, fname);
        
     numOfProjs = alg1Results{k}.projNum{bestAlg1Res(2,k)};
     angles = linspace(bestAlg1Res(2,k),179+bestAlg1Res(2,k),numOfProjs);
     R = radon(currImg, angles);
     ureconstruction = SIRTBound(R, angles, start, mask, 0.1, 100);
        fname=strcat('results/alg1SirtIncrAngles/bestRecs/uimg',num2str(k),'pn', num2str(alg1Results{k}.projNum{bestAlg1Res(2,k)}),'deg',num2str(bestAlg1Res(2,k)),'.png');
        imwrite(ureconstruction > 0.5, fname);
 end
 
 % Getting and saving the reconstructions of Alg2+Unc
 for k=1:22
     
    currImg=testImages{1}.images{k};  
    imgSize = size(currImg, 1);
    % * * * SIRT * * *
    mask=ones(imgSize,imgSize);
    start=zeros(imgSize,imgSize);
    start(:,:) = 0.5;
    % * * * *  * * * *   
    
    angles = alg2UncResults{k}.angles{1,bestUncRes(1,k)};
    P = radon(currImg, angles);
    reconstruction = SIRTBound(P, angles, start, mask, 0.1, 100);
    fname=strcat('results/alg2UncIncrAngles/bestRecs/img',num2str(k),'pn', num2str(alg2UncResults{k}.projNum{bestUncRes(1,k)}),'deg',num2str(bestUncRes(1,k)),'.png');
    imwrite(reconstruction > 0.5, fname);
    
    envMap = getUnc2(reconstruction);    
    fname = strcat('results/alg2UncIncrAngles/bestRecs/enMap',num2str(k),'pn', num2str(alg2UncResults{k}.projNum{bestUncRes(1,k)}),'deg',num2str(bestUncRes(1,k)),'.png');
    imwrite(envMap, fname);
    
    numOfProjs = alg2UncResults{k}.projNum{bestUncRes(2,k)};
     angles = linspace(bestUncRes(2,k),179+bestUncRes(2,k),numOfProjs);
     R = radon(currImg, angles);
     ureconstruction = SIRTBound(R, angles, start, mask, 0.1, 100);
        fname=strcat('results/alg2UncIncrAngles/bestRecs/uimg',num2str(k),'pn', num2str(alg2UncResults{k}.projNum{bestUncRes(2,k)}),'deg',num2str(bestUncRes(2,k)),'.png');
        imwrite(ureconstruction > 0.5, fname);
 end
 
 % Getting and saving the reconstructions of Alg2
 for k=1:22
     
    currImg=testImages{1}.images{k};  
    imgSize = size(currImg, 1);
    % * * * SIRT * * *
    mask=ones(imgSize,imgSize);
    start=zeros(imgSize,imgSize);
    start(:,:) = 0.5;
    % * * * *  * * * *
    
    
    angles = alg2Results{k}.angles{1,bestAlg2Res(k)};
    P = radon(currImg, angles);
    reconstruction = SIRTBound(P, angles, start, mask, 0.1, 100);
    fname=strcat('results/alg2SirtIncrAngles/bestRecs/img',num2str(k),'pn', num2str(alg2Results{k}.projNum{bestAlg2Res(1,k)}),'deg',num2str(bestAlg2Res(1,k)),'.png');
    imwrite(reconstruction > 0.5, fname);       
    
    numOfProjs = alg2Results{k}.projNum{bestAlg2Res(2,k)};
     angles = linspace(bestAlg2Res(2,k),179+bestAlg2Res(2,k),numOfProjs);
     R = radon(currImg, angles);
     ureconstruction = SIRTBound(R, angles, start, mask, 0.1, 100);
     fname=strcat('results/alg2SirtIncrAngles/bestRecs/uimg',num2str(k),'pn', num2str(alg2Results{k}.projNum{bestAlg2Res(2,k)}),'deg',num2str(bestAlg2Res(2,k)),'.png');
     imwrite(ureconstruction > 0.5, fname);
 end
 %% uniform reconstruct with projNum of best RME for Unc
  
minURMR = zeros(1,22);
minURMR(:,:) = 1.999;
for k=1:22
     
    currImg=testImages{1}.images{k};  
    imgSize = size(currImg, 1);
    % * * * SIRT * * *
    mask=ones(imgSize,imgSize);
    start=zeros(imgSize,imgSize);
    start(:,:) = 0.5;
    % * * * *  * * * *   
    projNum = alg2UncResults{k}.projNum{1,bestUncRes(1,k)};
    for kk=0:(180/projNum)
        angles = linspace(kk, 179+kk, projNum);
        P = radon(currImg, angles);
        reconstruction = SIRTBound(P, angles, start, mask, 0.1, 100);
        uniRMR=RMR(currImg, reconstruction>0.5);
        if minURMR(1,k)>uniRMR
            minURMR(1,k)=uniRMR;
        end
    end
 end
 
 %% collect all the best RME values into a matrix
 compRMEs=zeros(6,22);
 for k=1:22
     compRMEs(1,k)=alg1Results{1,k}.rmr{bestAlg1Res(1,k)};
     compRMEs(2,k)=alg1Results{1,k}.projNum{bestAlg1Res(1,k)};
     compRMEs(3,k)=alg2Results{1,k}.rmr{bestAlg2Res(1,k)};
     compRMEs(4,k)=alg2Results{1,k}.projNum{bestAlg2Res(1,k)};
     compRMEs(5,k)=alg2UncResults{1,k}.rmr{bestUncRes(1,k)};
     compRMEs(6,k)=alg2UncResults{1,k}.projNum{bestUncRes(1,k)};
 end
 
%% to count how many times Unc is better than uniform, alg1 or alg2
%  uncRMEs = zeros(4,22);
%  for k=1:22
%      uncRMEs(1,k) = alg2UncResults{1,k}.rmr{bestUncRes(1,k)};
%      uncRMEs(2,k) = alg2UncResults{1,k}.uRMR{bestUncRes(2,k)};
%      uncRMEs(3,k) = alg2Results{1,k}.rmr{bestAlg2Res(1,k)};
%      uncRMEs(4,k) = alg1Results{1,k}.rmr{bestAlg1Res(1,k)};
%  end
% form1=(uncRMEs(1,:)<uncRMEs(4,:));
% form2=(uncRMEs(1,:)<uncRMEs(3,:));
% form3=(uncRMEs(1,:)<uncRMEs(2,:));
% sum(form1)
% sum(form2)
% sum(form3)

%% compareGraph of Unc and uniform

figure;
set(gcf, 'Units', 'Normalized','OuterPosition', [0 0 1 1]);
for j=1:22
    dataToPlot=zeros(22,2);
    dataToPlot(:,1)=compRMEs(5,:)';
    dataToPlot(:,2)=compRMEs(1,:)';
end
    yyaxis left    
    bar(dataToPlot);
    ylabel('RME');
    yyaxis right
    plot(1:22,compRMEs(6,:),'+r');
    ylabel('Vetületek száma');
    legend('Unc','uniform');
    xlabel('Vizsgált fantom sorszáma');
    fname=sprintf('/results/compareUniformJaUnc.png');
    saveas(gcf,[pwd, fname]);    
