function plotImagesInMatrix(inputImages, oNum)
%inputImages a cell that can contains more set of results, as scrtiptAlg1
%   produces more set of results included the uniform reconstruction technic
%   with min and max projection numbers. So oNum is a parameter that says
%   which set from the cell should be used.

numOfImagesPerScale=length(inputImages{1}.images);
[x, y]=closestDivisor(numOfImagesPerScale);

nofScales = length(inputImages);

% for j=1:nofScales
j=oNum;
figure('Name', 'Init Step Size:7');
   %title(strcat('initial stepsize',string(inputImages{j}.initStep{1,5})));
   for i=1:length(inputImages{1}.images)
        subplot(x,y,i)
        imshow(inputImages{j}.images{i,3,2},[]);
        title({strcat('img',string(i));
                strcat('projNum:', string(length(inputImages{j}.angles{i,3,2},[])))
              });
   end
% end
end