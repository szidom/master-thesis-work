%%
addpath('TomoPhantoms');
addpath('Algorithm2');
addpath('utils');
files=dir('TomoPhantoms/*.tif');
testImages = cell(1,1);
nofImages = 1; %length(files); * * *

for j=1:nofImages
    testImages{1}.images{j} = imread(strcat('TomoPhantoms/',files(j).name));
end
testImages{1}.scale=length(testImages{1}.images{1});

nys = [0.1, 0.6 , 0.8, 1.0, 1.5];
initSteps = [5, 7, 10];

%%

% results2 = EBReconstruct(testImages, initSteps, 42, nys);
% function  [ results ] = EBReconstruct( testImages, initStep, maxNumOfProj, nys )

initStep = initSteps;
maxNumOfProj = 42;

%%

nofScales = length(testImages);
results = cell(1,length(testImages));
initAngle = 0;

k=1;
kk=1;
kkk=1;

% EbrPerformSingleImage(testImages{j}.images{k}, initStep(kkk), maxNumOfProj, nys(kk), initAngle );
% function [reconstruction, angles, stepSizes, errors, psnr] = EbrPerformSingleImage(inputImage, initStep, maxNumOfProj, ny, initAngle )

inputImage = testImages{j}.images{k};
initStep = initStep(kkk);
ny = nys(kk);

%%

imgSize = size(inputImage, 1);

% * * * SIRT * * *
mask=ones(imgSize,imgSize);
start=zeros(imgSize,imgSize);
start(:,:) = 0.5;
% * * * *  * * * *


angles = zeros(1, maxNumOfProj);
errors = zeros(1, maxNumOfProj);
angles(1) = initAngle;
angles(2) = initAngle + initStep;
P1 = radon(inputImage, angles(1));
P2 = radon(inputImage, angles(2));
    
 %%
 
  % * * * firstR = iradon([P1 P1], [angles(1) angles(1)], 'linear', imgSize) / 2;
   firstR = SIRTBound(P1, angles(1), start, mask, 0.1, 100);
  % * * * secondR = iradon([P2 P2], [angles(2) angles(2)], 'linear', imgSize) / 2;
  secondR = SIRTBound(P2, angles(2), start, mask, 0.1, 100);
  
 %%
 
E0 = log10(sumabs((firstR + secondR)/2 - firstR).^2);
errors(1)= E0;
prevRec = secondR + firstR;

E1 = log10(sumabs((prevRec)/2 - firstR).^2);
errors(2)= E1;
%Eref = mean(errors);

aNOP=1;
stepSizes=zeros(1, maxNumOfProj-1);
stepSizes(1) = angles(2)-angles(1);

%A cikkbeli algoritmus szerint ezt nem szamolja ujra minden iteracioban
Eref=mean(errors(1, 1:aNOP+1));
reconstruction = prevRec;
    
 %%
fig = figure,
while angles(aNOP+1) < (179 + initAngle) && aNOP+1<maxNumOfProj
    aNOP = aNOP + 1;
    deltaE = (errors(aNOP) - Eref)/errors(aNOP);
    avgStepSize = mean(stepSizes(1, 1:aNOP-1));
    deltau = avgStepSize*ny*deltaE;
    if deltau > 4
        deltau=4;
    end
    %itt initStep helyett az angles(2)<-upd! mar nem lesz igaz, mert
    %lehet hogy nem 0-tol idulunk...
    stepSizes(aNOP) = initStep - deltau;
    nextAngle=angles(aNOP) + stepSizes(aNOP);
    %itt mar novelni erdemes
    angles(aNOP+1)=nextAngle;
    R=radon(inputImage, nextAngle);
    
    % * * * actRec = iradon([R R], [nextAngle, nextAngle], 'linear', imgSize)/2;
     
 %   actRec = SIRTBound(R, nextAngle, start, mask, 0.1, 100);
   
 %   reconstruction = (prevRec + actRec);
 %   imshow(reconstruction, []);
    
    % * * * Ecurr = log10(sumabs((reconstruction/aNOP+1) - (prevRec/(aNOP))).^2);
    
    angToRec = angles(1:aNOP);
    R=radon(inputImage, angToRec);
    actRec = SIRTBound(R, angToRec, start, mask, 0.1, 100);
    
    % *****************
    % megjelenites es mentes mozgokepkent
    imshow(actRec, []);
    frame = getframe(fig);
    im{aNOP-1} = frame2im(frame);
    % *****************
    
    Ecurr = getUnc(R, actRec);
    
    % * * *
    
    errors(aNOP+1)=Ecurr;
%    prevRec=reconstruction;
    %x = input('IN')
end
%%

reconstruction=reconstruction/(aNOP);
    
mse=sum(sum((inputImage-reconstruction).^2))*1/imgSize^2;
psnr = 10*log10(1/mse);
%ha a vetuletek szama kisebb lenne mint az elore meghatarozott
%maximalis vetuletszam akkor ezeket az adatokat erdemes akkora tombbe
%athelyezni a kesobbi korrekt hiba, PSNR osszehasonlitashoz...
if(aNOP<maxNumOfProj)
    angles=angles(1:aNOP);
    errors=errors(1:aNOP);
    stepSizes=stepSizes(1:aNOP);
end

filename = 'testAnimation.gif';
for idx = 1:aNOP-1
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A, map, filename,'gif','LoopCount',Inf, 'DelayTime', 0.5);
    else
        imwrite(A, map, filename, 'gif', 'WriteMode', 'append', 'DelayTime', 0.5);
    end
end
