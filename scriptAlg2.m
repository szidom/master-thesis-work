%testImages = CreateTestImages(10, [256 512]);
currfile = mfilename('fullpath');
[pathstr,~,~]=fileparts(currfile);
cd (pathstr);

addpath('TomoPhantoms');
addpath('Algorithm2');
addpath('utils');
files=dir('TomoPhantoms/*.tif');
testImages = cell(1,1);
nofImages = length(files);

for j=1:nofImages
    testImages{1}.images{j} = imread(strcat('TomoPhantoms/',files(j).name));
end
testImages{1}.scale=length(testImages{1}.images{1});

nys = [0.1, 0.6 , 0.8, 1.0, 1.5];
initSteps = [5, 7, 10];
tic;
results2 = EBReconstruct(testImages, initSteps, 42, nys);
toc;

maxProjs=zeros(nofImages,1);
minProjs=maxProjs;
meanProjs=maxProjs;

for k=1:nofImages
    %itt vehetnenk a mean-t a kulso max/min helyett, de tul kicsi lenne az
    %elteres a ket ertek kozott (2-3 vetulet)
    maxProjs(k)=max(max(cellfun(@length, results2{1}.stepSizes(k,:,:))));
    minProjs(k)=min(min(cellfun(@length, results2{1}.stepSizes(k,:,:))));
%    meanProjs(k)=round((minProjs(k)+maxProjs(k))/2);
end
results2{2}=uniRec(testImages,minProjs);
results2{3}=uniRec(testImages,maxProjs);
%results2{4}=uniRec(testImages,meanProjs);

%% diagram kinyereshez hasznalhato kodok
%plotPSNRCh(results2);
%
%plotImagesInMatrix(results2,1);
%
%plotPieAngles(results2, 0.1, 5, 20);
%% tablazat adat kinyereshez hasznalt kodok:
% array2table(cellfun('length',results{1}.stepSizes(:,:,3)));
% cell2table(results{1}.PSNR(:,:,1));
% //uniform(max) projekciok szama
% cellfun('length',results2{3}{1,1}.angles)'; 
