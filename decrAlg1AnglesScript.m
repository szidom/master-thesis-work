currfile = mfilename('fullpath');
[pathstr,~,~]=fileparts(currfile);
cd (pathstr);

 addpath('TomoPhantoms');
 addpath('Algorithm1');
 addpath('utils');
 addpath('results');
files=dir('TomoPhantoms/*.tif');
testImages = cell(1,1);
nofImages=length(files);

for j=1:nofImages
    testImages{1}.images{j} = imread(strcat('TomoPhantoms/',files(j).name));
end
testImages{1}.scale=length(testImages{1}.images{1});


%selected parameters according to the results of scriptAlg1
ny=0.6;
initStep=10;
for j=1:nofImages
   for jj=0:179
      currImg=testImages{1}.images{j};     
      [~, angles, stepSizes, PSNR, rmr] = ...
          SrbPerformSingleImage(currImg, initStep, 42, ny, jj);
      %alg1Results{j}.images{jj+1} = reconstruction;
      alg1Results{j}.angles{jj+1} = angles;
      alg1Results{j}.stepSizes{jj+1} = stepSizes;
      alg1Results{j}.PSNR{jj+1} = PSNR;
      alg1Results{j}.rmr{jj+1} = rmr;


      %fetching uniform PSNR value for the same Image with same proj-num.
      numOfProjs = length(angles);      
      alg1Results{j}.projNum{jj+1} = numOfProjs;
      imgSize = size(currImg, 1);
      angles = linspace(jj,179+jj,numOfProjs);
      
      
    % * * * SIRT * * *
    mask=ones(imgSize,imgSize); 
    start=zeros(imgSize,imgSize); 
    start(:,:) = 0.5;
    % * * * *  * * * *
      
      R = radon(currImg, angles);
      %*** reconstruction=iradon(R,angles,'linear',imgSize);
      reconstruction = SIRTBound(R, angles, start, mask, 0.1, 100);
      alg1Results{j}.uRMR{jj+1}=RMR(currImg, reconstruction > 0.5);
      
    
%       mse=sum(sum((currImg-reconstruction).^2))*1/imgSize^2;
%       psnr = 10*log10(1/mse);
%       alg1Results{j}.uPSNR{jj+1}=psnr;
   end
end
%% Generate and save linegraphs as png, out of PSNR and uPSNR datas
x=1:180;
figure;
for j=1:nofImages
    yyaxis left
    plot(x,cell2mat(alg1Results{1,j}.projNum));
    ylabel('Number of projections');
    yyaxis right
    plot(x, cell2mat(alg1Results{1,j}.rmr),x, cell2mat(alg1Results{1,j}.uRMR));
    ylabel('R-M Error');
    legend('Num of Projs','Alg1','uniform');
    xlabel('Initial angle of projection');
    %fname='results/alg1GraphsDecrAngles';
    fname=sprintf('/results/alg1SirtIncrAngles/tendsImg%d.png',j);
    saveas(gcf,[pwd, fname]);    
end
save('results/alg1SirtIncrAngles/alg1Results.mat', 'alg1Results');
%% That is how to print out results
% figure; plot(x,cell2mat(alg1Results{1,2}.PSNR),x,cell2mat(alg1Results{1,2}.uPSNR));