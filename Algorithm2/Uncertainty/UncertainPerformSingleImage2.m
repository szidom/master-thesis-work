function [reconstruction, angles, stepSizes, errors, psnr, rmr] = UncertainPerformSingleImage2(inputImage, initStep, maxNumOfProj, ny, initAngle )
    
    imgSize = size(inputImage, 1);

    % * * * SIRT * * *
    mask=ones(imgSize,imgSize);
    start=zeros(imgSize,imgSize);
    start(:,:) = 0.5;
    % * * * *  * * * *
    
    angles = zeros(1, maxNumOfProj);
    errors = zeros(1, maxNumOfProj);
    angles(1) = initAngle;
    angles(2) = initAngle + initStep;
    % just for 1st reconsctruction
    firstAngles(1)=initAngle-initStep;
    firstAngles(2)=initAngle;
    firstAngles(3)=angles(2);
    
    P0 = radon(inputImage, initAngle-initStep);
    P1 = radon(inputImage, firstAngles(1:2));
    P2 = radon(inputImage, firstAngles);
    
    zeroR = SIRTBound(P0, firstAngles(1), start, mask, 0.1, 100);
    firstR = SIRTBound(P1, firstAngles(1:2), start, mask, 0.1, 100);
    secondR = SIRTBound(P2, firstAngles, start, mask, 0.1, 100);
        
    e1Map = getUnc2(firstR);
    E0 = log10(sumabs(e1Map - getUnc2(zeroR)).^2);
    errors(1)= E0;
    
    ePrevMap = getUnc2(secondR);
    E1 = log10(sumabs(ePrevMap - e1Map).^2);
    errors(2) = E1;

    aNOP=1;
    stepSizes = zeros(1, maxNumOfProj-1);
    stepSizes(1) = angles(2)-angles(1);
    
    Eref = (E0 + E1)/2;
    reconstruction = secondR;
    
    while angles(aNOP+1) < (179 + initAngle) && aNOP+1<maxNumOfProj
        aNOP = aNOP + 1;        
        deltaE = (errors(aNOP) - Eref)/errors(aNOP);
        avgStepSize = mean(stepSizes(1, 1:aNOP-1)); % kiiratni ennek az erteket. folyamatosan + e?, biztos?
        deltau = avgStepSize*ny*deltaE;
        if deltau > 4
            deltau = 4;
        elseif deltau < -4
            deltau = -4;
        end
        
        stepSizes(aNOP) = initStep - deltau;
        nextAngle = angles(aNOP) + stepSizes(aNOP);
                
        angles(aNOP+1)=nextAngle;
        R = radon(inputImage, angles(1:aNOP+1));
        
        reconstruction = SIRTBound(R, angles(1:aNOP+1), start, mask, 0.1, 100);
        Ecurr = getUnc2(reconstruction);
        errors(aNOP+1) = log10(sumabs(Ecurr - ePrevMap).^2);
        ePrevMap = Ecurr;
    end
        
    % RME- érték számítása a rekonstruált kép binarizaltjaval
    rmr=RMR(inputImage, reconstruction>0.5);
    %ha a vetuletek szama kisebb lenne mint az elore meghatarozott
    %maximalis vetuletszam akkor ezeket az adatokat erdemes akkora tombbe
    %athelyezni a kesobbi korrekt hiba, PSNR osszehasonlitashoz...
    if(aNOP<maxNumOfProj)
        angles=angles(1:aNOP);
        errors=errors(1:aNOP);
        stepSizes=stepSizes(1:aNOP);
    end
end
