%% Tesztkepek beolvasasa

files=dir('TomoPhantoms/*.tif');
testImages = cell(1,1);
nofImages=length(files);

for j=1:nofImages
    testImages{1}.images{j} = imread(strcat('TomoPhantoms/',files(j).name));
end

%% Getting the best Result of Alg2+Unc
bestUncRes=zeros(2,22);

for k=1:22
    minRMR=100.0;
    currRMR=0;
    minUrmr=10;
    currURMR=0;    
    for kk=1:180
        currRMR = alg2UncResults{1,k}.rmr{kk};
        if currRMR < minRMR
            minRMR=currRMR;
            bestUncRes(1,k)=kk;
        end
        currURMR = alg2UncResults{1,k}.uRMR{kk};
        if currURMR < minUrmr
            minUrmr=currURMR;
            bestUncRes(2,k)=kk;
        end
    end
end

% Getting the best result of Alg1
bestAlg1Res=zeros(2,22);
for k=1:22
    minRMR=10;
    currRMR=0;
    for kk=1:180
        if alg1Results{1,k}.projNum{kk} == alg2UncResults{1,k}.projNum{1,bestUncRes(1,k)}
            currRMR = alg1Results{1,k}.rmr{kk};
            if currRMR < minRMR
                minRMR = currRMR;
                bestAlg1Res(1,k) = kk;
            end
        end
    end
end

% Getting the best result of Alg2
%de az alg2 az meg nem volt teljesen jo mikor utoljara futott 
bestAlg2Res=zeros(2,22);
for k=1:22
    minRMR=1000;
    currRMR=0;    
    for kk=1:180
        if alg2Results{1,k}.projNum{kk} == alg2UncResults{1,k}.projNum{1,bestUncRes(1,k)}
            currRMR = alg2Results{1,k}.rmr{kk};
            if currRMR < minRMR
                minRMR = currRMR;
                bestAlg2Res(1,k) = kk;
            end
        end
    end
end

 
 %% collect all the best RME values into a matrix
 compRMEs=zeros(6,22);
 for k=1:22
     if bestAlg1Res(1,k) > 0
         compRMEs(1,k)=alg1Results{1,k}.rmr{bestAlg1Res(1,k)};
         compRMEs(2,k)=alg1Results{1,k}.projNum{bestAlg1Res(1,k)};
     end
     if bestAlg2Res(1,k) > 0
         compRMEs(3,k)=alg2Results{1,k}.rmr{bestAlg2Res(1,k)};
         compRMEs(4,k)=alg2Results{1,k}.projNum{bestAlg2Res(1,k)};
     end
     compRMEs(5,k)=alg2UncResults{1,k}.rmr{bestUncRes(1,k)};
     compRMEs(6,k)=alg2UncResults{1,k}.projNum{bestUncRes(1,k)};
 end
 
%% to count how many times Unc is better than uniform, alg1 or alg2
 uncRMEs = zeros(4,22);
 for k=1:22
     uncRMEs(1,k) = alg2UncResults{1,k}.rmr{bestUncRes(1,k)};
     if bestAlg2Res(1,k) > 0
        uncRMEs(3,k) = alg2Results{1,k}.rmr{bestAlg2Res(1,k)};
     end
     if bestAlg1Res(1,k) > 0
        uncRMEs(4,k) = alg1Results{1,k}.rmr{bestAlg1Res(1,k)};
     end
 end
form1=(uncRMEs(1,:)<uncRMEs(4,:));
form2=(uncRMEs(1,:)<uncRMEs(3,:));
sum(form1)
sum(form2)
