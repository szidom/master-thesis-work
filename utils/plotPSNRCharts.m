function plotPSNRCharts(inputImages)
    numOfImagesPerScale=length(inputImages{1}.images);
    nofScales = length(inputImages);
    %colors=['ymcgbwk'];
    % it returns with length of ny-s 
    dim2 = size(inputImages{1}.ny,2);
    dataToPlot=zeros(numOfImagesPerScale, dim2+1 ); 
     
    for j=1:nofScales
     figure('Name', 'Algorithm1');
         figure;
         %bar(1:numOfImagesPerScale, cellfun(@mean, unInputImages{j}.errors), 'r');
         dataToPlot(:, 1) = cellfun(@mean, unInputImages{j}.PSNR);
         stringforLegends=['Uniform Projections'];
         for k=1:length(inputImages{j}.ny(1,:))      
            datatocollect = zeros(1, numOfImagesPerScale);
            for i=1:numOfImagesPerScale
                datatocollect(i) = cellfun(@mean, inputImages{j}.PSNR(i,k));                
            end                
            dataToPlot(:,k+1) = datatocollect';
            stringforLegends=[stringforLegends, strcat('\eta=', string(inputImages{j}.ny{1,k})) ];
         end
         bar(dataToPlot);
         title(strcat(string(inputImages{j}.scale),' scale Images'));         
         xlabel('Number of Images');
         ylabel('PSNR');
         legend(stringforLegends);
     end
end
