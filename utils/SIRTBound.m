% [0, 1] tarom�nyra korl�tozott SIRT algoritmus
%       (MATLAB vet�t�ssel, �s visszavet�t�ssel)
%
%  result = SIRTBound(sinogram, angleList, start, mask, stopCrit, maxCycle)
%
% Kimenet: a rekonstrukci� eredm�nye
%
% Param�terek:
%   - sinogram:     a rekonstrukci�hoz haszn�lhat� vet�letek
%   - angleList:    a vet�t�si ir�nyok list�ja
%   - start:        kiindul� pixel�rt�kek vektora
%   - mask:         a v�ltoztathat� k�ppontokat le�r� maszk
%   - stop_crit:    1. meg�ll�si felt�tel - ha a v�ltoz�s kissebb mint egy
%                   k�sz�b
%   - max_cycle:    iter�ci�k maxim�lis sz�ma


function [result, cycle, changesum] = SIRTBound(sinogram, angleList, start, mask, stopCrit, maxCycle)

    s_i = size(start);
    projCount = size(sinogram, 2);
    
    %tempIm = iradon(radon(mask, angleList), angleList, 'linear', 'None', 1, s_i(1));
    if size(angleList, 2) == 1
        tempIm = iradon(radon(mask, [angleList angleList]), [angleList angleList], 'linear', 'None', 1, s_i(1));
    else
        tempIm = iradon(radon(mask, angleList), angleList, 'linear', 'None', 1, s_i(1));
    end
    normFactor = max(tempIm(:));
    result = start;
    cycle = 0;

    while true
        if size(angleList, 2) == 1
            projDiff = sinogram - radon(result, [angleList angleList]);
            change = iradon(projDiff, [angleList angleList], 'linear', 'None', 1, s_i(1)) / normFactor;
        else
            projDiff = sinogram - radon(result, angleList);
            change = iradon(projDiff, angleList, 'linear', 'None', 1, s_i(1)) / normFactor;
        end
        
        result = result + change.*mask;
        
        result = max(result, 0);
        result = min(result, 1);
        
        changesum = (sum(sum(change.^2)))^.5;
        cycle = cycle + 1;

        if (maxCycle > 0) && (maxCycle <= cycle)
            break;
        end
        
        if (changesum < stopCrit)
            break;
        end
    end
    
end