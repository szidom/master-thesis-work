function results = uniRec( testImages, nofProjs )

    results = cell(length(testImages));
    
        results{1}.scale = testImages{1}.scale;
        for k=1 : length(testImages{1}.images)
            [reconstruction, angles, PSNR] = performSingleImage(testImages{1}.images{k}, nofProjs(k) );
            results{1}.image{k}=reconstruction;
            results{1}.angles{k}=angles;
            results{1}.PSNR{k}=PSNR;
        end
end

function [reconstruction, angles, psnr] = performSingleImage(inputImage, maxNumOfProj )
    
    imgSize = size(inputImage, 1);
    angles = linspace(0,179,maxNumOfProj);
    
    R = radon(inputImage, angles);
    reconstruction=iradon(R,angles,'linear',imgSize);
    
    mse=sum(sum((inputImage-reconstruction).^2))*1/imgSize^2;
    psnr = 10*log10(1/mse);    
end
