function plotPieAngles( res, ny, initStep, numOfImg )
%PLOTPIEANGLES Plots selected projection angles in form of pie chart
%   res - is the result of the projection selection algorithm
%   ny - (double value)selected ny value
%   initStep - (integer value)selected initStep
%   numOfImg - (integer value)selected image from the image set
global nys initSteps;
%nys = [0.1, 0.6 , 0.8, 1.0, 1.5];
%initSteps = [5, 7, 10];
eta=nys==ny; %instead of "find" it is recommended logical indexing 'couse its faster
is=initSteps==initStep;

labels = cellstr(string(round(res{1}.angles{numOfImg,eta,is})));
angles = res{1}.stepSizes{numOfImg,eta,is};
figure;

positionVector1 = [0.1, 0.2, 0.3, 0.3];
subplot('Position',positionVector1)
imshow(res{1}.images{numOfImg,eta,is},[]);
title({'Number of projections for selected' ...
    strcat(' \eta=', string(ny),' and initial angle=',string(initStep),' is :', string(length(angles))) ...
    strcat('PSNR = ', string(res{1}.PSNR{numOfImg,eta,is}),'  image:',string(numOfImg) )});

positionVector2 = [0.5, 0.1, 0.4, 0.7];
subplot('Position',positionVector2)
pie(angles./360.0, labels);
end

