function plotErrorCharts(inputImages, unInputImages)
    numOfImagesPerScale=length(inputImages{1}.images);
    nofScales = length(inputImages);
    colors=['ymcgbwk'];
    % it returns with length of ny-s 
    dim2 = size(inputImages{1}.ny,2);
    dataToPlot=zeros(numOfImagesPerScale, dim2+1 ); 
     
    for j=1:nofScales
     %figure('Name', string(strcat('size of the images:',string(inputImages{j}.scale(1)))));
         figure;
         %bar(1:numOfImagesPerScale, cellfun(@mean, unInputImages{j}.errors), 'r');
         dataToPlot(:, 1) = cellfun(@mean, unInputImages{j}.errors);
         stringforLegends=['Uniform Projections'];
         for k=1:length(inputImages{j}.ny(1,:))      
            datatocollect = zeros(1, numOfImagesPerScale);
            for i=1:numOfImagesPerScale
                datatocollect(i) = cellfun(@mean, inputImages{j}.errors(i,k));                
            end                
            dataToPlot(:,k+1) = datatocollect';
            stringforLegends=[stringforLegends, strcat('ny=', string(inputImages{j}.ny{1,k})) ];
         end
         bar(dataToPlot);
         title(strcat(string(inputImages{j}.scale),' scale Images'));         
         xlabel('Number of Images');
         ylabel('Error');
         legend(stringforLegends);
     end
end
