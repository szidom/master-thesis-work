function [ unc ] = getUnc( sinogram, recon)
    % % float getUncertaintyReal(float * sinogram, int lineCount, int projectionCount,  float * recon, int h, int w) {
    h=size(recon, 1);
    w=size(recon, 2);
    sumUncertainty = 0;
    entropyMap=zeros(h,w);
    sinoInt = 0;
	
    for i=1:h
        for j=1:w
            if recon(i,j) > 0 && recon(i,j) < 1
                entropyMap(i,j) = -(recon(i,j) * log2(recon(i,j)) + (1 - recon(i,j))*log2(1 - recon(i,j)));
                sumUncertainty = sumUncertainty + entropyMap(i,j);
            else
                entropyMap(i,j) = 0;
            end
        end
    end

	for i=1:size(sinogram,1)
        for j=1:size(sinogram,2)
            sinoInt=sinoInt+sinogram(i,j);
        end
    end
	sinoInt=sinoInt/size(sinogram,2);

	unc = sumUncertainty/sinoInt;
end