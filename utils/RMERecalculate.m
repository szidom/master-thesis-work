%%RME recalculate script
files=dir('TomoPhantoms/*.tif');
testImages = cell(1,1);
nofImages=length(files);

for j=1:nofImages
    testImages{1}.images{j} = imread(strcat('TomoPhantoms/',files(j).name));
end

% * * * SIRT * * *
mask=ones(imgSize,imgSize); 
start=zeros(imgSize,imgSize); 
start(:,:) = 0.5;
% * * * *  * * * *

for k=1:nofImages
    for kk=1:180
        inputImage = testImages{1}.images{1,k};
        angles = alg2UncResults{1,k}.angles{1,kk};
        P = radon(inputImage, angles);
        reconstruction = SIRTBound(P, angles, start, mask, 0.1, 100);
        rmr=RMR(inputImage, reconstruction>0.5);
        alg2UncResults{1,k}.rmr{1,kk}=rmr;
        %% uniform RME
        angles = linspace(kk,179+kk, alg2UncResults{1,k}.projNum{1,kk});
         
        R = radon(inputImage, angles);
%       ureconstruction=iradon(R,angles,'linear',imgSize);
        ureconstruction = SIRTBound(R, angles, start, mask, 0.1, 100);
        alg2UncResults{1,k}.uRMR{1,kk}=RMR(inputImage, ureconstruction > 0.5);
    end
end