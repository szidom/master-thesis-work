currfile = mfilename('fullpath');
[pathstr,~,~]=fileparts(currfile);
cd (pathstr);

addpath('TomoPhantoms');
addpath('Algorithm2/Uncertainty');
addpath('utils');
addpath('results');
files=dir('TomoPhantoms/*.tif');
testImages = cell(1,1);
nofImages=length(files);

for j=1:nofImages
    testImages{1}.images{j} = imread(strcat('TomoPhantoms/',files(j).name));
end
testImages{1}.scale=length(testImages{1}.images{1});

%selected parameters according to the results of scriptAlg2
ny=0.8;
initStep=10;
for j=1:nofImages
   for jj=0:179
      currImg=testImages{1}.images{j};     
      [~, angles, stepSizes, errors, PSNR, rmr] = ...
          UncertainPerformSingleImage2(currImg, initStep, 42, ny, jj);
      %alg1Results{j}.images{jj+1} = reconstruction;
      alg2UncResults{j}.angles{jj+1} = angles;
      alg2UncResults{j}.stepSizes{jj+1} = stepSizes;
      alg2UncResults{j}.PSNR{jj+1} = PSNR;
      alg2UncResults{j}.rmr{jj+1} = rmr;
      alg2UncResults{j}.errors{jj+1} = errors;
    
      %fetching uniform PSNR value for the same Image with same proj-num.
      numOfProjs = length(angles);
      alg2UncResults{j}.projNum{jj+1} = numOfProjs;
      imgSize = size(currImg, 1);
      angles = linspace(jj, 179+jj, numOfProjs);
    
     % * * * SIRT * * *
     mask=ones(imgSize,imgSize); 
     start=zeros(imgSize,imgSize); 
     start(:,:) = 0.5;
     % * * * *  * * * *
      
      R = radon(currImg, angles);
      %ureconstruction=iradon(R,angles,'linear',imgSize);
      ureconstruction = SIRTBound(R, angles, start, mask, 0.1, 100);
      alg2UncResults{j}.uRMR{jj+1}=RMR(currImg, ureconstruction>0.5 );
      
%       mse=sum(sum((currImg-ureconstruction).^2))*1/imgSize^2;
%       psnr = 10*log10(1/mse);
%       alg2UncResults{j}.uPSNR{jj+1}=psnr;
   end
   % print out the progress
   fprintf('progress is %d %% \n',round((j/nofImages)*100) );
end
%% Generate and save linegraphs as png, out of PSNR and uPSNR datas
x=1:180;
figure;
for j=1:nofImages
    yyaxis left
    plot(x,cell2mat(alg2UncResults{1,j}.projNum));
    ylabel('Num of Projs');
    yyaxis right
    plot(x, cell2mat(alg2UncResults{1,j}.rmr),x, cell2mat(alg2UncResults{1,j}.uRMR));
    ylabel('R-M Error');
    legend('Number of Projections','Alg2+Unc','uniform');
    xlabel('Initial angle of projection');
    %fname='results/alg1GraphsDecrAngles';
    fname=sprintf('/results/alg2UncIncrAngles/tends180Img%d.png',j);
    saveas(gcf,[pwd, fname]);
end
save('results/alg2UncIncrAngles/alg2UncResults.mat', 'alg2UncResults');

% The new ones for the report:
x=1:180;
figure;
set(gcf, 'Units', 'Normalized','OuterPosition', [0 0 1 1]);
for j=1:nofImages
    yyaxis left
    plot(x, cell2mat(alg2UncResults{1,j}.rmr), '.g', x, cell2mat(alg2UncResults{1,j}.uRMR), 'xb');
    ylabel('RME');    
    yyaxis right
    plot(x,cell2mat(alg2UncResults{1,j}.projNum),'+r');
    ylabel('Vetületek száma');
    legend('Unc','uniform','Vetület szám');
    xlabel('Kezdeti vetületi szög');
    %fname='results/alg1GraphsDecrAngles';
    fname=sprintf('/results/alg2UncIncrAngles/forReport%d.png',j);
    saveas(gcf,[pwd, fname]);
end

%% That is how to print out results
% figure; plot(x,cell2mat(alg1Results{1,2}.PSNR),x,cell2mat(alg1Results{1,2}.uPSNR));