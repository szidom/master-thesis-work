function [reconstruction, angles, stepSizes, psnr, rmr] = SrbPerformSingleImage(inputImage, initStep, maxNumOfProj, ny, initAngle )
    % in the beginning we take the first 2 projection in order to calculate
    % spectral richness and then we continue in a loop
    imgSize = size(inputImage, 1);
    
    % * * * SIRT * * *
    mask=ones(imgSize,imgSize); % kell 1 mask, mi az egesz kepet vesszuk
    start=zeros(imgSize,imgSize); 
    start(:,:) = 0.5;
    % * * * *  * * * *
    
    angles = zeros(1, maxNumOfProj);
    richnesses = zeros(1, maxNumOfProj);
    angles(1) = initAngle;
    angles(2) = initAngle + initStep;
    
    P0 = radon(inputImage, angles(1));
    %This is the way in matlab to perform one step of FBP for a certain angle
    %*** b0 = iradon([P0 P0], [angles(1), angles(1)], 'none',imgSize)/2;
    b0 = SIRTBound(P0, angles(1), start, mask, 0.1, 100);
    
    b2d=fft2(b0);
    
    % W-t eleg egyszer letrehozni , meg I-t is hiszen a projekcio merete
    % mindig ugyanaz lesz
    W=dftmtx(imgSize);
    WT=W';
    B0=W*b2d ;%*WT;
    
    %since we start with vertical projection we take the horizontal line of
    % image in the furier space which is going to be the middle line of the
    % furier image
%    tmp=abs(B0(imgSize/2,1:imgSize));
    %J0=sum(sum(tmp),2);
    %these values just need to be summed up
%    J0=sum(tmp);
    [X,Y]=GetRotLine(imgSize, angles(1));
    
    J0=0;
        for l=1:imgSize
            if(X(l)~=0)% && Y(l)~=0)% -tulzott elovigyazatossag
                J0=J0+abs(B0(X(l),:) * WT(:,Y(l)));
            end
        end  

%   Eddig igy volt nezzuk most maskepp    
%     R = radon(inputImage, [angles(1), angles(2)]);
%     prevRec = iradon(R, [angles(1), angles(2)], 'none',imgSize);
%     b2d=fft2(prevRec);
    R=radon(inputImage, angles(2));
    %***actRec = iradon([R R], [angles(2), angles(2)], 'none', imgSize)/2;
     actRec = SIRTBound(R, angles(2), start, mask, 0.1, 100);
    
    prevRec=b0 + actRec;
    b2d=fft2(prevRec/2);

    B1=W*b2d;
    
    %egy korabbi csunya megoldas az egyenes menten kivett ertekek
    %G=imresize(imrotate(I, initStep),[imgSize,imgSize],'box');
    %G=full(spones(G));
    
    [X,Y]=GetRotLine(imgSize, angles(2));
    
    J1=0;
        for l=1:imgSize
            if(X(l)~=0)% && Y(l)~=0)% -tulzott elovigyazatossag
                J1=J1+abs(B1(X(l),:) * WT(:,Y(l)));
            end
        end      
%     indices=sub2ind([imgSize,imgSize], X, Y );
%     
%     %tmp=abs(B1.*G);
%     %J1=sum(sum(tmp),2);
%     %J1=0;
%     J1=sum(abs(B1(indices)));ez a vegso rekonstrukcio. itt hogy legyen? gondolom egyseges kellene
%     %for l=1:imgSize
%     %    if(X(l)~=0) %&& Y(l)~=0) -tulzott elovigyazatossag
%     %        J1=J1+abs(B1(X(l),Y(l)));
%     %    end
%     %end
   
    richnesses(1)= J0;
    richnesses(2)= J1;
    
    %actual number of projections
    aNOP=2;
    stepSizes=zeros(1, maxNumOfProj);
    stepSizes(1) = initStep;
    
    while (angles(aNOP) < (179 + initAngle)) && aNOP<maxNumOfProj
        Javg=mean(richnesses(1:aNOP));        
        deltaJ = (richnesses(aNOP) - Javg)/richnesses(aNOP);
        avgStepSize = mean(stepSizes(1:aNOP-1));
        deltau = avgStepSize*ny*deltaJ;
        if deltau>4
            deltau=4;
        end
        
        stepSizes(aNOP) = avgStepSize - deltau;
        nextAngle=angles(aNOP) + stepSizes(aNOP);
        %itt mar novelni erdemes
        aNOP = aNOP + 1;
        angles(aNOP) = nextAngle;
        R = radon(inputImage, angles(aNOP));
        %*** actRec = iradon([R R], [nextAngle, nextAngle], 'none', imgSize)/2;
        actRec = SIRTBound(R, nextAngle, start, mask, 0.1, 100);
        %bn = iradon(R, angles(1:aNOP), 'none', imgSize);
        prevRec=prevRec+actRec;
        %bn=reconstruction'*R;
        b2d = fft2(prevRec/aNOP);
        
        % Bn=W*b2d*WT;
        % a masodik matrix szorzas kikuszobolheto
        Bn=W*b2d;
        %G=imresize(imrotate(I, angles(aNOP)),[imgSize,imgSize],'box');
        %G=full(spones(G));
        %tmp=abs(Bn.*G);
        [X,Y]=GetRotLine(imgSize, angles(aNOP));
        J1=0;
        for l=1:imgSize
            if(X(l)~=0) %&& Y(l)~=0) -tulzott elovigyazatossag
                %J1=J1+abs(Bn(X(l),Y(l)));
                J1=J1+abs(Bn(X(l),:) * WT(:,Y(l)));
%egy masik megkozelites lehetne az elemenkenti szorzas, szoval csak a
%szukseges elemeket szamolni
%                  wb2d=W(X(l),:)*b2d(:,Y(l))*WT(Y(l),:);
%                 Bn=wb2d.*WT(X(l),1:Y(l));
%                 J1=sum(sum(abs(Bn)));

            end
        end        
        richnesses(aNOP)=J1;
    end
    aNOP=aNOP-1;
    %ha a vetuletek szama kisebb lenne mint az elore meghatarozott
    %maximalis vetuletszam akkor ezeket az adatokat erdemes akkora tombbe
    %athelyezni a kesobbi korrekt hiba osszehasonlitashoz...
    if(aNOP<maxNumOfProj)
        angles=angles(1:aNOP);
        stepSizes=stepSizes(1:aNOP);
    end
    
    %'none' filterrel negativ psnr ertekek szuletnek.
    %Ez az utolsó rekonstrukció csak a psnr számításhoz kell.
    %legyen a tobbivel az eredmenyek osszehasonlitasa vegett, ezert is
    %linear filtert hasznaltunk
    R = radon(inputImage, angles);
    %*** reconstruction=iradon(R, angles,'linear', imgSize);
    reconstruction = SIRTBound(R, angles, start, mask, 0.1, 100);
    % RME- érték számítása a rekonstruált kép binarizaltjaval
    rmr=RMR(inputImage, reconstruct > 0.5);
    %reconstruction=prevRec/(aNOP);
    
    %angles
    mse=sum(sum((inputImage-reconstruction).^2))*1/imgSize^2;
    psnr = 10*log10(1/mse);
    
end
