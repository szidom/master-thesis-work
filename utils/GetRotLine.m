function [X,Y]=GetRotLine(size, alpha)
%This function is gives you back the coordinates of a line rotated in angle
%alpha. If it is greater than 179 then we transform it into the interval 0-179
%the algorithm works on square matrix with dimension of even-numbered
%inputs params: size - the dimension of the matrix,
%               alpha - angle of the needed line
%output params: X - the corresponding x coordinates of the line 
%               y - the corresponding y coordinates of the line
%       on a [size x size] matrix
    if alpha >= 180
        alpha = alpha-180;
        if alpha >= 180
            alpha = alpha-180;
        end
    end
    
    if (alpha >= 0 && alpha < 45) || (alpha > 135 && alpha < 180)
        y = (-(size/2-1):size/2); % mintavetelezeshez a pontok
        x=round(-y/cot(deg2rad(alpha)));% x*cos(a)+y*sin(a)=0
    else
        x = (-(size/2-1):size/2);
        y=round(-x*cot(deg2rad(alpha)));
    end
    
    X=zeros(size,1);
    Y=zeros(size,1);
    for i=1:size
        if y(i)+(size/2) >= 1 && y(i)+(size/2) <= size
           X(i)=x(i)+(size/2);
           Y(i)=y(i)+(size/2);
        end
    end
end