% x=1:180;
% figure;
% set(gcf, 'Units', 'Normalized','OuterPosition', [0 0 1 1]);
% for j=1:nofImages
%     yyaxis left
%     plot(x,cell2mat(alg1Results{1,j}.projNum),x,cell2mat(alg2Results{1,j}.projNum),x,cell2mat(alg2UncResults{1,j}.projNum));
%     ylabel('Num of Projections');
%     yyaxis right
%     plot(x, cell2mat(alg1Results{1,j}.rmr),x, cell2mat(alg2Results{1,j}.rmr),x,cell2mat(alg2UncResults{1,j}.rmr));
%     ylabel('RME value');
%     legend('Alg1','Alg2','Unc','Alg1','Alg2','Unc');
%     xlabel('Initial angle of projection');
%     %fname='results/alg1GraphsDecrAngles';
%     fname=sprintf('/results/compareAlgs/tendsImg%d.png',j);
%     saveas(gcf,[pwd, fname]);    
% end
% save('results/compareAlgs/resultsData.mat', 'alg1Results','alg2Results','alg2UncResults');

%% This part is meant to generate and save the RME and Num. of Proj. graphs separately 
x=1:180;
figure;
set(gcf, 'Units', 'Normalized','OuterPosition', [0 0 1 1]);
for j=1:nofImages
    plot(x,cell2mat(alg1Results{1,j}.projNum), '+r', ...
        x,cell2mat(alg2Results{1,j}.projNum),'xb',...
        x,cell2mat(alg2UncResults{1,j}.projNum),'.g');
    ylabel('Vetületek száma');
    legend('Alg1','Alg2','Unc');
    xlabel('Kezdeti vetületi szög');
    fname=sprintf('/results/compareAlgs/projNums%d.png',j);
    saveas(gcf,[pwd, fname]);   
    
    plot(x,cell2mat(alg1Results{1,j}.rmr), '+r', ...
        x,cell2mat(alg2Results{1,j}.rmr),'xb',...
        x,cell2mat(alg2UncResults{1,j}.rmr),'.g');
    ylabel('RME');
    legend('Alg1','Alg2','Unc');
    xlabel('Kezdeti vetületi szög');
    fname=sprintf('/results/compareAlgs/RMEs%d.png',j);
    saveas(gcf,[pwd, fname]);    
end