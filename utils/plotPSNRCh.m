function plotPSNRCh(inputImages)
    numOfImagesPerScale=length(inputImages{1}.angles);
    % it returns with length of ny-s 
    dim2 = size(inputImages{1}.ny,2);
    dataToPlot=zeros(numOfImagesPerScale, dim2+3 ); 
     
         figure;
         stringforLegends=[];
         for k=1:length(inputImages{1}.ny(1,:))      
            collectedData = zeros(1, numOfImagesPerScale);
            for i=1:numOfImagesPerScale
                collectedData(i) = max(cellfun(@max, inputImages{1}.PSNR(i,k,:)));                
            end                
            dataToPlot(:,k) = collectedData';
            stringforLegends=[stringforLegends, strcat('\eta=', string(inputImages{1}.ny{1,k})) ];
         end
         dataToPlot(:,k+1) =cellfun(@mean, inputImages{2}{1}.PSNR);
         dataToPlot(:,k+2) =cellfun(@mean, inputImages{3}{1}.PSNR);
         %dataToPlot(:,k+3) =cellfun(@mean, inputImages{4}{1}.PSNR);
         stringforLegends=[stringforLegends, 'Min uniform', 'Max uniform'];
         
         bar(dataToPlot);
         title(strcat(string(inputImages{1}.scale),' scale Images'));         
         xlabel('Number of Images');
         ylabel('PSNR');
         legend(stringforLegends);
end
